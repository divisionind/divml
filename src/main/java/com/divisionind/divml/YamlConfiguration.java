/*
 * DIVISION CONFIDENTIAL
 * __________________________
 *
 *  2015-2018 Division Industries
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of Division Industries
 * and its suppliers, if any.  The intellectual and technical concepts contained herein are proprietary
 * to Division Industries and its suppliers and may be covered by U.S. and Foreign Patents, patents
 * in process, and are protected by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior written permission is obtained
 * from Division Industries.
 */

package com.divisionind.divml;

import org.ho.yaml.Yaml;

import java.io.*;
import java.util.HashMap;
import java.util.List;

/**
 * Created by drew6017 on 3/12/2017.
 */
public class YamlConfiguration extends HashMap<String, Object> {

    public static final int DEFAULT_RESOURCE_EXTRACT_BUFFER = 2048;

    public String getString(String key) {
        return (String)get(key);
    }

    public int getInteger(String key) {
        return (int)get(key);
    }

    public short getShort(String key) {
        return (short)get(key);
    }

    public long getLong(String key) {
        return (long)get(key);
    }

    public List<String> getAsStringList(String key) {
        return (List<String>)get(key);
    }

    public HashMap<String, Object> getAsMap(String key) {
        return (HashMap<String, Object>) get(key);
    }

    public void save(File f) throws FileNotFoundException {
        Yaml.dump(this, f, true);
    }

    public static YamlConfiguration load(File f) throws FileNotFoundException {
        return Yaml.loadType(f, YamlConfiguration.class);
    }

    public static void extractResource(String path, File out, int buffer_size) throws IOException {
        InputStream in = YamlConfiguration.class.getResourceAsStream(path);
        if (in == null) throw new IOException("Internal resource not found.");
        OutputStream o = new FileOutputStream(out);
        byte[] buff = new byte[buffer_size];
        int read;
        while ((read = in.read(buff)) != -1) o.write(buff, 0, read);
        in.close();
        o.flush();
        o.close();
    }

    public static void extractResource(String path, File out) throws IOException {
        extractResource(path, out, DEFAULT_RESOURCE_EXTRACT_BUFFER);
    }
}
